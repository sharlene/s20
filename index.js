// activity code
let number = prompt("Please input a number");
console.log(`The number you provided is ${number}`);

for(; number >= 50; number--){
  if(number === 50) {
    console.log("The current value is at 50. Terminating the loop.");
    break;
  } else if(number % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  } else if(number % 5 === 0) {
    console.log(number);
  }
}

//stretch goal code
let word = "supercalifragilisticexpialidocious";
console.log(word);

let newWord = word.replace(/[aeiou]/g, "");
console.log(newWord);
